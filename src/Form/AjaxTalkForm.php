<?php

namespace Drupal\ajax_commands_talk\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
// Ajax related classes.
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;

/**
 * Class AjaxTalkForm.
 */
class AjaxTalkForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ajax_talk_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['message'] = [
      '#type' => 'textfield',
      '#placeholder' => $this->t('Escreva uma mensagem legal'),
      '#title' => $this->t('Mensagem'),
    ];

    // Exemplo de botão com ajax.
    $form['submit'] = [
      '#type' => 'button',
      '#value' => t('Exibir mensagem'),
      '#ajax' => [
        'callback' => '::ajaxButtonSubmit'
        // Usando Drupal\ajax_commands_talk\Form\AjaxTalkForm::ajaxButtonSubmit
        // funcionaria também se ajaxButtonSubmit fosse um método static.
      ]
    ];

    return $form;
  }

  public static function ajaxButtonSubmit(array $form, FormStateInterface $form_state) {
    $message = $form_state->getValue('message');

    $response = new AjaxResponse();
    $response->addCommand(new AlertCommand(
      t('Mensagem: @msn' , ['@msn' => $message])
    ));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
  }
}
