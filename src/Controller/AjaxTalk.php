<?php

namespace Drupal\ajax_commands_talk\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
// Ajax related classes.
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\PrependCommand;

/**
 * Class AjaxTalk.
 */
class AjaxTalk extends ControllerBase {

  /**
   * Ajax Commands Talk demo page.
   */
  public function page() {
    $page = [];

    // Exemplo de formulário com ajax submit.
    $form = \Drupal::formBuilder()->getForm('\Drupal\ajax_commands_talk\Form\AjaxTalkForm');
    $page['form'] = $form;

    // Exemplo de link com Ajax.
    $page['link'] = [
      '#type' => 'link',
      '#title' => t('Exemplo de como mostrar Drupal Messages'),
      '#url' => Url::fromRoute('ajax_commands_talk.ajax_message'),
      '#attributes' => [
        'class' => ['use-ajax'],
      ],
    ];

    return $page;
  }

  /**
   * Shows the request time in a message.
   */
  public static function message() {
    \Drupal::messenger()->addMessage(t('Callback executado em: @time',
      ['@time' => REQUEST_TIME]
    ));
    $status_messages = ['#type' => 'status_messages'];
    $messages = \Drupal::service('renderer')->renderRoot($status_messages);

    $response = new AjaxResponse();

    $response->addCommand(new RemoveCommand('div[data-drupal-messages]'));
    $response->addCommand(new PrependCommand('.region.region-highlighted', $messages));

    return $response;
  }
}
